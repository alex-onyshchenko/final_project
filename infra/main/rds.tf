resource "aws_db_subnet_group" "mysqldb_subnet" {
  name = "mysqldb-subnet"
  description = "RDS subnet group"
  subnet_ids = [aws_subnet.main-private-1.id,aws_subnet.main-private-2.id]
}


resource "aws_db_parameter_group" "mysql_param_group" {
  name = "mysql-parameters-group"
  family = "mysql5.7"
  description = "MySQL parameter group"
}

resource "aws_db_instance" "mysql" {
    allocated_storage    = 20
    storage_type         = "gp2"
    engine               = "mysql"
    engine_version       = "5.7"
    instance_class       = "db.t2.micro"
    identifier = "mysqldatabase"
    name = "petclinic"
    username = var.RDS_USERNAME
    password = var.RDS_PASSWORD
    parameter_group_name = aws_db_parameter_group.mysql_param_group.name
    db_subnet_group_name = aws_db_subnet_group.mysqldb_subnet.name
    vpc_security_group_ids = [aws_security_group.rds_sg.id]
    availability_zone = aws_subnet.main-private-1.availability_zone
    backup_retention_period = 10
    skip_final_snapshot = true
    tags = {
        Name = "MySQL Server"
    }
}

output "rds_endpoint" {
  value = aws_db_instance.mysql.endpoint
}