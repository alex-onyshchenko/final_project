provider "aws" {
  region = "eu-west-2"
}

terraform {
  backend "s3" {
    region = "eu-west-2"
    bucket = "latyn4ikdede"
    key = "terraform_backup.tfstate"
  }
}