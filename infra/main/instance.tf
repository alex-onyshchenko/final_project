resource "aws_instance" "app_instance" {
  ami = var.DOCKER_EC2_AMI
  instance_type = var.EC2_TYPE
  key_name = "aws_key"
  vpc_security_group_ids = [aws_security_group.instance_sg.id]
  subnet_id = aws_subnet.main-public-1.id
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      host        = self.public_ip
      private_key = file(var.SSH_KEY_PATH_APP_INSTANCE)
      user        = var.APP_INSTANCE_USER
    }
    inline = ["docker run -d  -e MYSQL_DATABASE_PATH='${aws_db_instance.mysql.endpoint}' -p 8080:8080 latyn4ik/my_pet_clinic:latest"]
  }
  tags = {
    Name = "Application Server"
  }
}

resource "aws_iam_role" "ec2_s3_access_role" {
  name = "CustomRoleForEC2"
  description = "myCustomRole-AllAccess"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}


output "public_app_ip" {
  value = aws_instance.app_instance.public_ip
}